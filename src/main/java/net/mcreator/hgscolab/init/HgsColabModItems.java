
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.hgscolab.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.world.item.Item;

import net.mcreator.hgscolab.item.RadiationArmorItem;
import net.mcreator.hgscolab.item.PistolplowItem;
import net.mcreator.hgscolab.item.PPSH41Item;
import net.mcreator.hgscolab.item.OronArmorItem;
import net.mcreator.hgscolab.item.OmonrussianArmorItem;
import net.mcreator.hgscolab.item.MEDKITItem;
import net.mcreator.hgscolab.item.BulletItem;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class HgsColabModItems {
	private static final List<Item> REGISTRY = new ArrayList<>();
	public static final Item ORON_ARMOR_HELMET = register(new OronArmorItem.Helmet());
	public static final Item ORON_ARMOR_CHESTPLATE = register(new OronArmorItem.Chestplate());
	public static final Item ORON_ARMOR_LEGGINGS = register(new OronArmorItem.Leggings());
	public static final Item ORON_ARMOR_BOOTS = register(new OronArmorItem.Boots());
	public static final Item RADIATION_ARMOR_HELMET = register(new RadiationArmorItem.Helmet());
	public static final Item RADIATION_ARMOR_CHESTPLATE = register(new RadiationArmorItem.Chestplate());
	public static final Item RADIATION_ARMOR_LEGGINGS = register(new RadiationArmorItem.Leggings());
	public static final Item RADIATION_ARMOR_BOOTS = register(new RadiationArmorItem.Boots());
	public static final Item PPSH_41 = register(new PPSH41Item());
	public static final Item BULLET = register(new BulletItem());
	public static final Item OMONRUSSIAN_ARMOR_HELMET = register(new OmonrussianArmorItem.Helmet());
	public static final Item OMONRUSSIAN_ARMOR_CHESTPLATE = register(new OmonrussianArmorItem.Chestplate());
	public static final Item OMONRUSSIAN_ARMOR_LEGGINGS = register(new OmonrussianArmorItem.Leggings());
	public static final Item OMONRUSSIAN_ARMOR_BOOTS = register(new OmonrussianArmorItem.Boots());
	public static final Item PISTOLPLOW = register(new PistolplowItem());
	public static final Item MEDKIT = register(new MEDKITItem());

	private static Item register(Item item) {
		REGISTRY.add(item);
		return item;
	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Item[0]));
	}
}
