
package net.mcreator.hgscolab.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.food.FoodProperties;

public class MEDKITItem extends Item {
	public MEDKITItem() {
		super(new Item.Properties().tab(CreativeModeTab.TAB_FOOD).stacksTo(8).rarity(Rarity.UNCOMMON)
				.food((new FoodProperties.Builder()).nutrition(8).saturationMod(5f).alwaysEat()

						.build()));
		setRegistryName("medkit");
	}
}
